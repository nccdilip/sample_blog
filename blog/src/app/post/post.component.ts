import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  BlogList :Array<any> = [];
  constructor(private appService: AppService) { }

  ngOnInit(): void {
    this.appService.getBlogList().subscribe((res: any) => {
      if(res.status != 400){
        this.BlogList = res.data;
      } else {
        alert(res.message);
      }
    })
  }
  deleteBlog(id: string, i: number) {
    this.appService.deleteBlog(id).subscribe((res: any)=>{
      if(res.status != 400){
        this.BlogList.splice(i,1);
      } else {
        alert(res.message);
      }
    })
  }
}
