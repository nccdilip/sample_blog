import {  Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class AppService {

  apiUrl = 'http://localhost:2001/api/';

  constructor(
    private http: HttpClient
    ) {
      //this.apiUrl = this.dataService.siteUrl+"/api/";
    }
    getBlogList() {
        return this.http.get<Response>(this.apiUrl+'blogList');
    }
    addOrUbdateBlog(data: any) {
        return this.http.post<Response>(this.apiUrl+'addOrUbdateBlog',data);
    }
    getBlog(id: string) {
        return this.http.get<Response>(this.apiUrl+'blog/'+id);
    }
    deleteBlog(id: string) {
      return this.http.delete<Response>(this.apiUrl+'blog/'+id);
  }
}
