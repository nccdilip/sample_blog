import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AppService } from '../app.service';
import { Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {
  formdata = new FormGroup({
    title: new FormControl("", Validators.compose([
      Validators.required
    ])),
    author: new FormControl("", Validators.compose([
      Validators.required
    ])),
    post: new FormControl("", Validators.compose([
      Validators.required
    ]))
  })
  submitted :boolean = false;
  blogId : string | null = '';
  constructor(private appService: AppService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.blogId = this.activatedRoute.snapshot.paramMap.get('id');
    if(this.blogId) {
      this.appService.getBlog(this.blogId).subscribe((res : any) =>{
        if(res.status != 400){
          this.formdata.patchValue(res.data);
        } else {
          alert(res.message);
        }
      })
    }

  }
  onClickSubmit(value: any){
    this.submitted = true;
    if(this.formdata.status !== "VALID")
      return;

    if(this.blogId)
      value._id = this.blogId;
    
    this.appService.addOrUbdateBlog(value).subscribe((res : any)=>{
      if(res.status != 400){
        this.router.navigate(['post']);
      } else {
        alert(res.message);
      }
    })
  }

}
