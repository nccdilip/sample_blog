import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { PostComponent } from './post/post.component';
import { NewComponent } from './new/new.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'post', component: PostComponent },
  { path: 'post/new', component: NewComponent },
  { path: 'post/edit/:id', component: NewComponent },
  { path: '',  redirectTo: '/home', pathMatch: 'full' },
  { path: '**',  component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
