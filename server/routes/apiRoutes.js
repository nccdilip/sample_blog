var express = require('express'),
blogControllers = require('../controllers/blogControllers');

callRouter = express.Router();

callRouter.get('/blogList',blogControllers.getBlogList);
callRouter.post('/addOrUbdateBlog',blogControllers.addOrUbdateBlog);
callRouter.get('/blog/:blogId',blogControllers.getBlog);
callRouter.delete('/blog/:blogId',blogControllers.deleteBlog);

module.exports = callRouter;