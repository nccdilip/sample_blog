var Blog = require('../models/blogSchema'),
	config = require('../config');

var blogBlock = {};

blogBlock.getBlogList = function (req, res, next) {
	Blog.find().exec(function (err, blogs) {
		if (err) {
			res.send(200).json(({ message: "Error is Getting The Data", status: 400 }))
		}
		res.status(200).json({ data: blogs , status: 200 }); return;
	});
};
blogBlock.addOrUbdateBlog = function (req, res, next) {
    var data = req.body;
    if(!data.title || !data.author || !data.post){
		res.status(200).json({message:"send the required Data" ,status: 400}); return;
	}
    if(data._id){
		Blog.findOneAndUpdate({_id:data._id}, data, { new : true },function(err, place){
			if(err){
				res.status(200).json({message:"Error in saving Data" ,status: 400}); return;
			}
			
			res.status(200).json({message:"Data Updated", status: 201}); return;
		});
	} else {
		var blog = Blog(data);
		blog.save(function (err) {
			if(err){
				console.log(err);
				res.status(200).json({message:"Error in saving Data" ,status: 400}); return;
			}
			res.status(200).json({message:"Data Added", status: 201}); return;
		});
	}
};
blogBlock.getBlog = function (req, res, next) {
    var blogId = req.params.blogId;
    Blog.findById(blogId).exec(function (err, blogs) {
		if (err) {
			res.send(200).json(({ message: "Error is Getting The Data", status: 400 }))
		}
		res.status(200).json({ data:  blogs, status: 200 }); return;
	});
};

blogBlock.deleteBlog = function (req, res, next) {
    var blogId = req.params.blogId;
    Blog.remove({ _id: blogId }, function(err) {
        if (err) {
			res.send(200).json(({ message: "Error is Getting The Data", status: 400 }))
		}
		res.status(200).json({message:"Data Deleted ", status: 201}); return;
    });
};
module.exports = blogBlock;