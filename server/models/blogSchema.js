var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var BlogSchema = new Schema({
    title: String,
    author: String,
    post: String,
},{timestamps: true});

module.exports = mongoose.model('Blog', BlogSchema);
