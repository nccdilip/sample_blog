var express = require("express"),
	mongoose = require('mongoose'),
	bodyParser = require('body-parser'),
    http = require('http'),
	config = require('./config');

mongoose.connect(config.database);

var app = express();

var apiRouter = require('./routes/apiRoutes.js');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(function (req, res, next) { 
	//console.log(req.headers['user-agent']);
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
	res.setHeader('Access-Control-Allow-Credentials', true);
	next();
});

app.get('/test', function (req, res) {
	res.send('test');
});

app.use('/api', apiRouter);

app.use(express.static(__dirname + "/public"));
app.use(/^((?!(api)).)*/, (req, res) => {
	res.sendFile(__dirname + "/public/index.html");
});


var httpServer = http.createServer(app);

httpServer.listen(2001);
