## Technologies can be used:

- HTML5
- CSS
- Bootstrap
- Anguler
- Node js
- Express js
- MongoDB

## Installation

how to run the code

```sh
cd server
npm install
node server.js
```

> Use URL http://localhost:2001/ for blog

## Folder


| Folder |  |
| ------ | ------ |
| blog |  Front end (Anguler) |
| server | Back end ( node ) |

## To run the Anguler

```sh
cd blog
npm install
node server.js
ng serve
```

> Use URL http://localhost:4200/ to run Anguler